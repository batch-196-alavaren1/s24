// console.log("Let the hunt begin.")

/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 4 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosophy 101", "Social Sciences 201"],
}


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosophy 401", "Natural Sciences 402"],
}


/*Debug and Update*/

// function introduce(student){

// 	console.log("Hi! I'm " + student.name + "." + " I am " + student.age + " years old.")
// 	console.log("I study the following courses: " + student.classes)

// }

// introduce(student1);
// introduce(student2);

const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`)
	console.log(`I study the following courses ${student.classes}.` )

}
introduce(student1);
introduce(student2);



// function getCube(num){

// 	console.log(Math.pow(num,3));

// }

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){
	console.log(num);
})


// let numsSquared = numArr.map(function(num){

// 	return num ** 2;

//   }
// )
// console.log(numsSquared);


let numsSquared = numArr.map((num)=>num**2);
console.log(numsSquared);

/*CORRECT ANSWER
const getCube = (num) => console.log(num ** 3)
let numArr = [15,16,32,21,21,2]
numArr.forEach((num)=>console.log(num));
let numsSquared = numArr.map((num)=>num**2);
console.log(numsSquared);
*/



/*2. Class Constructor*/

class Guild {
	constructor(userName,role,guildName,level){

		this.userName = userName;
		this.role = role;
		this.guildName = guildName;
		this.level = level;
	}
}
let guild1 = new Guild("Rafaela","support","LOL",12);
let guild2 = new Guild("Pharsa","mage","CHX",15);

console.log(guild1);
console.log(guild2);